import 'package:flutter/material.dart';
import 'stream.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}
class HomePageState extends State<StatefulWidget> {
  List<String> images= [];
  StreamSend iS = StreamSend();
  @override
  void initState(){
    iS.fetchImage();
    changeText();
    super.initState();
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Image',
      home: Scaffold(
        appBar: AppBar(title: Text('Image')),
        body: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: images.length,
          itemBuilder: (BuildContext context, int index){
            return Card(
                child: Container(
                  child: Image.network(images[index]),
                )
            );
          },
        ),
      ),
    );
  }

  changeText() async{
    iS.getImage().listen((eventText) {
      setState(() {
        images.add(eventText);
      });
    });
  }
}
